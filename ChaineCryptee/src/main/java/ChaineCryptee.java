/**
 * Javadoc Thibaud Larcher chaine cryptee
 */
public class ChaineCryptee {

    private String chainecrypte;
    private int decalage;

    /**
     * Constructeur de la classe Chaine Cryptee
     * il y a une exception sur le faite que la chaine soit null
     * @param chaine La chaine claire
     * @param decal le nombre de decalage
     */
    private ChaineCryptee(String chaine, int decal){
        if(chaine==null) {
            chaine = "";
        }
        this.decalage=decal;
        this.chainecrypte=Crypte(chaine);
    }

    /**
     *
     * @return on return la chaine claire
     */
    public String Decrypte(){
        return chainecrypte;
    }

    /**
     * Fonction qui encrypte le message claire
     * @return Le message cryptee
     */
    public String Crypte(String chaine){
         this.chainecrypte=chaine;
        for (int i=0 ;i < chaine.length();i++) {
            StringBuffer bf = new StringBuffer(chainecrypte);
            bf.setCharAt(i,decaleCaractere(chaine.charAt(i), this.decalage));
            chainecrypte = bf.toString();
        }
        return chainecrypte;
    }

    /**
     * Fonction de décalage de chaque caractère
     * @param c
     * @param decalage
     * @return
     */
    private static char decaleCaractere(char c, int decalage) {
        return (c < 'A' || c > 'Z')? c : (char)(((c - 'A' + decalage) % 26) + 'A');
    }

    public static ChaineCryptee deCryptee(String msg, int i){
        return new ChaineCryptee(msg,i);
    }

    public static ChaineCryptee deEnClair(String msg, int i){
        return new ChaineCryptee(msg,i);
    }

}
