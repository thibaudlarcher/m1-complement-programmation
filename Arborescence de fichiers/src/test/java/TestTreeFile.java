import org.junit.jupiter.api.*;

public class TestTreeFile {

    /**
     * Test all fonction of Tree File
     * @throws fileAlreadyExisting
     */
    @Test
    public void NormalUse() throws fileAlreadyExisting {
        RegularFile f1 = new RegularFile(12,"fichier1");
        RegularFile f2 = new RegularFile(13,"fichier2");
        Directory racine = new Directory("repracine");
        Directory rep1 = new Directory("rep1");
        racine.addRegularFile(f1);
        racine.addDirectory(rep1);
        rep1.addRegularFile(f2);
        System.out.println(racine.getSize());
    }

    /**
     * Test if you can add Directory in himself
     * @throws fileAlreadyExisting
     */
    @Test
    public void AddHimself() throws fileAlreadyExisting {
        Directory racine = new Directory("repracine");
        racine.addDirectory(racine);
    }

    /**
     * Test if you can add father's Directory in son's Directory
     * @throws fileAlreadyExisting
     */
    @Test
    public void AddSonInFather() throws fileAlreadyExisting {
        Directory racine = new Directory("repracine");
        Directory rep1 = new Directory("rep1");
        Directory rep2 = new Directory("rep2");
        racine.addDirectory(rep1);
        rep1.addDirectory(rep2);
        rep2.addDirectory(racine);
    }

}
