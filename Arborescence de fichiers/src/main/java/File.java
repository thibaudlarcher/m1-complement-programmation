public abstract class File  {
    /**
     * All abstract method
     */
    protected String name;
    public abstract int getSize();
    public abstract String getName();
    public abstract boolean directoryCheck(Directory rep1);
}
