import java.util.ArrayList;

public class Directory extends File {
    /**
     * @param name All repository have a name
     * @param listFile All repository have many File (RegularFile or Directory)
     */

    private ArrayList<File> listFile;

    /**
     * Constructor of Directory Class
     * @param name Chose a name of Directory
     */
    public Directory(String name) {
        this.name = name;
        listFile = new ArrayList<File>();
    }

    /**
     * This method add a simple RegularFile in his Directory
     * @param file retrieve existing File type RegularFile
     * @return Check if an RegularFile or Directory have a same name
     * @throws fileAlreadyExisting Raise an error to Java.Exception if RegularFile or Directory have a same name
     */
    public boolean addRegularFile(RegularFile file) throws fileAlreadyExisting {
        for (int i = 0; i < listFile.size(); i++) {
            if (listFile.get(i).getName().equals(file.getName())) {
                throw new fileAlreadyExisting("File is already existing");
            }
        }
        listFile.add(file);
        return true;
    }

    /**
     * This method add a simple Directory in his Directory
     * @param directory retrieve existing File type Directory in all son's Directory
     * @return Check if an RegularFile or Directory have a same name
     * @throws fileAlreadyExisting Raise an error to Java.Exception if RegularFile or Directory have a same name
     */
    public  boolean addDirectory(Directory directory) throws fileAlreadyExisting {
        if (this.getName().equals(directory.getName())) {
            throw new fileAlreadyExisting("File is already existing");
        } else if (!(this.directoryCheck(directory))) {
            throw new fileAlreadyExisting("File is already existing");
        }
        for (int i = 0; i < listFile.size(); i++) {
            if (listFile.get(i).equals(directory)) {
                throw new fileAlreadyExisting("File is already existing");
            }
        }
        listFile.add(directory);
        return true;
    }

    /**
     * This method check all Directory Tree
     * @param directory Check son's Directory and his father
     * @return Check if an RegularFile or Directory have a same name
     */
    @Override
    public boolean directoryCheck(Directory directory) {
        if (directory.listFile.isEmpty()) {
            return true;
        } else {
            for (int i = 0; i < directory.listFile.size(); i++) {
                if (directory.listFile.get(i).getName() == this.getName()) {
                    return false;
                } else if (!directory.listFile.get(i).directoryCheck(directory)) {
                    return false;
                }
            }
            return true;
        }
    }

    /**
     * This method retrieve his name
     * @return name of this Directory
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * This method retrieve his size
     * @return size of this Directory
     */
    @Override
    public int getSize() {
        if (this.listFile.isEmpty()) {
            return 0;
        }
        else {
            int somme = 0;
            for (int i = 0; i < this.listFile.size(); i++) {
                somme = somme + this.listFile.get(i).getSize();
            }
            return somme;
        }
    }
}
