public class RegularFile extends File {

    /**
     * @param name name of this RegularFile
     * @param size size of RegularFile
     */
    private int size;

    /**
     * Constructor of this class
     * @param size size of this RegularFile
     * @param name name of this RegularFile
     */
    public RegularFile(int size, String name) {
        this.name = name;
        this.size = size;
    }

    /**
     * Check if RegularFile have other file but RegularFile can't have File inside him
     * @param directory Chose a Directory
     * @return always true
     */
    @Override
    public boolean directoryCheck(Directory directory) {
        return true;
    }

    /**
     * This method retrieve his name
     * @return name of this RegularFile
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * This method retrieve his size
     * @return size of this RegularFile
     */
    @Override
    public int getSize() {
        return size;
    }
}
