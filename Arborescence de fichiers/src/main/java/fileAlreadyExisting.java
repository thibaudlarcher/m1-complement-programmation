public class fileAlreadyExisting extends Exception {
    /**
     * this method raise an exeption if File is already existing in a List
     * @param message
     */
    public fileAlreadyExisting(String message) {
        super(message);
    }
}
