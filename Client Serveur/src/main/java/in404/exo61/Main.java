import in404.exo61.serveur.*;
import in404.exo61.client.*;
public class Main {

    public static void main (String[] args) {
        Serveur serv1 = new Serveur();
        Client client1 = new Client("Alice");
        Client client2 = new Client("Bob");
        Client client3 = new Client("Charlie");

        client1.seConnecter(serv1);
        client2.seConnecter(serv1);
        client3.seConnecter(serv1);
        client3.envoyer("bonjour");
    }
}
