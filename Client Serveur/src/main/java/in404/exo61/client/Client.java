package in404.exo61.client;
import in404.exo61.serveur.*;

public class Client {
    public String nom;

    public Client(String nom){
        this.nom=nom;
    }

    public Boolean seConnecter(Serveur serv){
        if(serv.connecter(this)){
            return true;

        }
        return false;
    }

    public void envoyer(String message){
        System.out.println(this.nom +":"+ message);
    }

    public void recevoir(String message){
        System.out.println(message);
    }
}
