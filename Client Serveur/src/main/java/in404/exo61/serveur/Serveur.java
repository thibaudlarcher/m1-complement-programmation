package in404.exo61.serveur;
import in404.exo61.client.*;
import java.util.ArrayList;


public class Serveur {
    public ArrayList <Client> lst;
    public Serveur(){
        lst = new ArrayList<Client>();
    }

    public boolean connecter(Client client){
        for(int i =0; i<lst.size();i++){
           if(lst.get(i)==client){
               return false;
           }
           else{
               lst.add(client);
               return true;
           }
        }
    return false;
    }

    public String diffuser(String message){
        for(int i =0; i<lst.size();i++){
            lst.get(i).recevoir(message);
        }
        return "Serveur : ok";
    }
}
