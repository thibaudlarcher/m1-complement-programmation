/**
 * Exception qui gère le dépassement du MAXIMUM définit dans la class MoteurRPN.
 */
public class ExceedingMaximumException extends Exception {

    public ExceedingMaximumException(final String message) {
        super(message);
    }
}
