import java.util.Stack;

/**
 * Classe du MoteurRPN.
 */
public class MoteurRPN {
    /**
     * @param MAX_VALUE elle correspond à la valeur max à ne pas dépasser sinon elle va raise l'erreur ExceedingMaximum.
     * @param MIN_VALUE elle correspond à la valeur min à ne pas dépasser sinon elle va raise l'erreur ExceedingMinumum.
     * @param pileOperande correspond à la pile des opérandes pour pouvoir calculer les équations.
     */
    private static final  float MAX_VALUE = 999;
    private static final  float MIN_VALUE = 0;
    private Stack<Float> pileOperande;

    /**
     * Dans le constructeur on initialise uniquement la pile.
     */
    public MoteurRPN() {
        pileOperande = new Stack<Float>();
    }

    /**
     * Fonction qui permet le calcule des opérations, elle va calculer 2 opérandes 2 a 2 grace à l'énumération Opération.
     * Elle va en même temps les enlever de la pile et replacer le résultat obtenue.
     * @param operation Correspond à l'énum des opération possible décrit dans l'énum Opération.
     * @throws ExeptionMissingToken
     * @throws ExceedingMaximumException
     * @throws ExceedingMinimumException
     * @throws ZeroDivisionException
     */
    public void applicationOperationSurOperande(final Operation operation) throws ExeptionMissingToken,
            ExceedingMaximumException, ExceedingMinimumException, ZeroDivisionException {
        float operand1 = 0, operand2 = 0;
        if (pileOperande.isEmpty()) {
            throw new ExeptionMissingToken();
        } else {
            operand1 = pileOperande.pop();
            if (pileOperande.isEmpty()) {
                throw new ExeptionMissingToken();
            } else {
                operand2 = pileOperande.pop();
            }
        }
        pileOperande.push(operation.eval(operand1, operand2));

    }

    /**
     * Fonction qui permet l'ajout dans la pile des Opérandes.
     * On veille a ce que les exceptions soit garanties.
     * @param operande On ajoute l'opérande récupérée dans la saisie
     * @throws ExceedingMaximumException
     * @throws ExceedingMinimumException
     */
    public void addInStack(final float operande)throws ExceedingMaximumException, ExceedingMinimumException {
        if (Math.abs(operande) >= MAX_VALUE) {
            throw new ExceedingMaximumException("out of MAX_VALUE");
        } else if (Math.abs(operande) <= MIN_VALUE) {
            throw new ExceedingMinimumException("Out of MIN_VALUE");
        } else {
            pileOperande.push(operande);
        }

    }

    /**
     * Fonction qui permet de récupérer la valeur max.
     * @return MAX
     */
    public static float getMaxValue() {
        return MAX_VALUE;
    }

    /**
     * Fonction qui permet de récupérer la valeur min.
     * @return MIN
     */
    public static float getMinValue() {
        return MIN_VALUE;
    }

    /**
     * Elle permet d'afficher ce que l'on a dans la pile.
     * @return string
     */
    @Override
    public String toString() {
        return pileOperande.toString();
    }
}
