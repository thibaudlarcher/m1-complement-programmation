/**
 * Exception qui gère l'envoie dans la saisiRPN un token qui n'existe pas dans le programme.
 */
public class IllegalTokenException extends Exception {
    public IllegalTokenException(final String message) {
        super(message);
    }
}
