import java.util.Scanner;

public class SaisieRPN {
    /**
     * Fonction de la saisie des calcules. ELle va checker si on envoie des nombres ou des Opérations.
     * Ainsi elle peut faire la différence entre les deux types.
     * Si l'on essaie de mettre un caractère inconnu au programme cela nous donne un erreur
     * @throws ExceedingMinimumException
     * @throws ExceedingMaximumException
     * @throws ZeroDivisionException
     * @throws ExeptionMissingToken
     */
    SaisieRPN() throws ExceedingMinimumException, ExceedingMaximumException, ZeroDivisionException,ExeptionMissingToken{
        System.out.println("Veuiller saisir une opération :");
        boolean exit = false;
        MoteurRPN calculatrice = new MoteurRPN();
        StringBuilder affiche = new StringBuilder();
        while (!exit){
            Scanner scanner = new Scanner(System.in);
            String string = scanner.nextLine();
            String sline[] = string.split(" ");
            for (int i = 0; i < sline.length; i++) {
                if (sline[i].equals("+") || sline[i].equals("-") || sline[i].equals("*") || sline[i].equals("/")){
                    if (sline[i].equals("+")) {
                        Operation Op = Operation.PLUS;
                        calculatrice.applicationOperationSurOperande(Op);
                        affiche.append(sline[i]+" ");
                    }
                    if (sline[i].equals("-")) {
                        Operation Op = Operation.MOINS;
                        calculatrice.applicationOperationSurOperande(Op);
                        affiche.append(sline[i]+" ");
                    }
                    if (sline[i].equals("*")) {
                        Operation Op = Operation.MULT;
                        calculatrice.applicationOperationSurOperande(Op);
                        affiche.append(sline[i]+" ");
                    }
                    if (sline[i].equals("/")) {
                        Operation Op = Operation.DIV;
                        calculatrice.applicationOperationSurOperande(Op);
                        affiche.append(sline[i]+" ");
                    }
                    System.out.println(affiche);
                }
                else{
                    if (sline[i].equals("EXIT")) {
                        System.out.println("Résultat : "+calculatrice.toString());
                        exit = true;
                    }
                    else{
                        calculatrice.addInStack(Float.parseFloat(sline[i]));
                        affiche.append(sline[i]+" ");
                        System.out.println(affiche);
                    }
                }
            }
        }
    }
}
