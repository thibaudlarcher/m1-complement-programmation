/**
 * Exception qui gère si il y a un token dans l'envoie à la saisie.
 */
public class ExeptionMissingToken extends Exception {
    public ExeptionMissingToken() {
        System.err.println("Missing Symbole or Number");
    }
}
