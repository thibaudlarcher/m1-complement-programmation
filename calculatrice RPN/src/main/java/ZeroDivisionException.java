/**
 * Exception qui gère la division par 0
 */
public class ZeroDivisionException extends Exception{
    public ZeroDivisionException(String message) {
        super(message);
    }
}
