/**
 * Fonction de main pour la calculatrice RPN.
 */
public enum CalculatriceRPN {
    /**
     * Fonction qui appel les fonctions si dessous.
     */
    MAIN;

    /**
     * Voici le Main dans une class Enumeration, elle va lancer la saisie de la calculatrice RPN avec chacune des exceptions.
     */
    public static void calculatriceRPN() {
        try {
            SaisieRPN saisie = new SaisieRPN();
        } catch (ExeptionMissingToken exeptionMissingToken) {
            exeptionMissingToken.printStackTrace();
        } catch (ExceedingMaximumException exceedingMaximumException) {
            exceedingMaximumException.printStackTrace();
        } catch (ExceedingMinimumException exceedingMinimumException) {
            exceedingMinimumException.printStackTrace();
        } catch (ZeroDivisionException zeroDivisionException) {
            zeroDivisionException.printStackTrace();
        } /*catch (IllegalTokenException illegalTokenException) {
            illegalTokenException.printStackTrace();
        }*/
    }

    /**
     *  Elle va appeler la fonction au dessus pour avec un lancement de programme.
     * @param args ce sont les paramètres lors de l'écriture dans le terminal.
     */
    public static void main(final String args[]) {
        MAIN.calculatriceRPN();
    }
}
