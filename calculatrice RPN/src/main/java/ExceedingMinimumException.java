/**
 * Exception qui gère le dépassement du MINIMUM définit dans la class MoteurRPN.
 */
public class ExceedingMinimumException extends Exception {

    public ExceedingMinimumException(final String message) {
        super(message);
    }
}
