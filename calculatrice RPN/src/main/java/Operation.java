/**
 * Class d'énumération des différentes opérations sur les chiffres.
 */
public enum Operation {
    /** Permet de faire l'addition/la soustraction/la multiplication/la division entre deux opérandes.
     * @param PLUS
     * @param MOINS
     * @param MULT
     * @param DIV Il y a une exception particulière celle de la division par 0 qui n'est pas possible
     */
    PLUS("+") {
        @Override
        public float eval(final float operand1, final float operand2) throws ExceedingMinimumException, ExceedingMaximumException {
            float result = 0;
            result =  operand2 + operand1;

            if (Math.abs(result) > MoteurRPN.getMaxValue()) {
                throw new ExceedingMaximumException("Out of MAX_VALUE");
            } else if (Math.abs(result) < MoteurRPN.getMinValue()) {
                throw new ExceedingMinimumException("Out MIN_VALUE");
            } else {
                return result;
            }
        }
    },
    MOINS("-") {
        @Override
        public float eval(final float operand1, final float operand2) throws ExceedingMaximumException, ExceedingMinimumException {
            float result = 0;
            result = operand2 - operand1;

            if (Math.abs(result) > MoteurRPN.getMaxValue()) {
                throw new ExceedingMaximumException("Out of MAX_VALUE");
            } else if (Math.abs(result) < MoteurRPN.getMinValue()) {
                throw new ExceedingMinimumException("Out MIN_VALUE");
            } else {
                return result;
            }
        }
    },
    MULT("*") {
        @Override
        public float eval(final float operand1, final float operand2) throws ExceedingMaximumException, ExceedingMinimumException {
            float result = 0;
            result = operand2 * operand1;

            if (Math.abs(result) > MoteurRPN.getMaxValue()) {
                throw new ExceedingMaximumException("Out of MAX_VALUE");
            } else if (Math.abs(result) < MoteurRPN.getMinValue()) {
                throw new ExceedingMinimumException("Out MIN_VALUE");
            } else {
                return result;
            }
        }
    },
    DIV("/") {
        @Override
        public float eval(final float operand1, final float operand2) throws ExceedingMinimumException, ExceedingMaximumException,
                ZeroDivisionException {
            if (operand1 == 0) {
                throw new ZeroDivisionException("Divide by 0 is impossible");
            }
            float result = 0;
            result = operand2 / operand1;
            if (Math.abs(result) > MoteurRPN.getMaxValue()) {
                throw new ExceedingMaximumException("Out of MAX_VALUE");
            } else if (Math.abs(result) < MoteurRPN.getMinValue()) {
                throw new ExceedingMinimumException("Out MIN_VALUE");
            } else {
                return result;
            }
        }
    };

    private String symbole;

    /**
     * Constructeur qui va simplement récupérer le symbole que l'on lui attribue.
     * @param sym
     */
    Operation(String sym) {
        this.symbole = sym;
    }

    /**
     * Fonction qui permet l'évaluation entre deux opérandes donc de permettre au autre class java de faire des calcules.
     * @param operand1
     * @param operand2
     * @return
     * @throws ExceedingMinimumException
     * @throws ExceedingMaximumException
     * @throws ZeroDivisionException
     */
    public abstract float eval(float operand1, float operand2) throws ExceedingMinimumException, ExceedingMaximumException,
            ZeroDivisionException;
}
