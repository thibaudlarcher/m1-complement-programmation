import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class CompteBancaireTest {

    @Test
    public void creationComptesanssolde(){
        CompteBancaire A =new CompteBancaire();

        assertEquals(A.getSolde(),0);
    }

    @Test
    public void creationCompteavecsoldepositif(){
        try {
            CompteBancaire A =new CompteBancaire(1000);
        }catch (ExceptionSoldePositif e){};

    }

    @Test
    public void creationCompteavecsoldenégatif(){
        try {
            CompteBancaire A =new CompteBancaire(-1000);
        }catch (ExceptionSoldePositif e){};
    }

    @Test
    public void debittest(){
        try {
            CompteBancaire A =new CompteBancaire();
            try {
                A.credit(1000);
            }catch (ExceptionMontantnegatif e){};
            try {
                A.debit(1200);
            }catch (ExceptionMontantnegatif e){};

        }catch (ExceptionSoldePositif e){};

    }

    @Test
    public void virementtest(){
        try {
            CompteBancaire A = new CompteBancaire(1000);
            CompteBancaire B = new CompteBancaire();
            try {
                A.virement(B,300);
                A.getSolde();
                B.getSolde();
            }catch (ExceptionMontantnegatif e){};
        }catch (ExceptionSoldePositif e){};

    }
}