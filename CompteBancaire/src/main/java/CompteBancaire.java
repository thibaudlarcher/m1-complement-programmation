public class CompteBancaire {
    private int Solde;

    public CompteBancaire(){
        this.Solde=0;
    }

    public CompteBancaire(int montant) throws ExceptionSoldePositif{
        if(montant < 0){
            throw new ExceptionSoldePositif();
        }
        else
            this.Solde=montant;
    }

    public int getSolde(){
        System.out.println("le solde est de :"+Solde);
        return this.Solde;
    }

    public void credit(int montant) throws ExceptionMontantnegatif{
        if(montant<0){
            throw new ExceptionMontantnegatif();
        }
        else
            this.Solde=this.Solde+montant;
        System.out.println("le crédit à bien été effectué");
    }

    public void debit(int montant) throws ExceptionMontantnegatif, ExceptionSoldePositif{
        if(montant<0){
            throw new ExceptionMontantnegatif();
        }
        if (this.Solde-montant < 0){
            throw new ExceptionSoldePositif();
        }
        else
            this.Solde=this.Solde-montant;
        System.out.println("le débit à bien été effectué");
    }

    public void virement(CompteBancaire A, int montant)throws ExceptionMontantnegatif{
        if(montant<0){
            throw new ExceptionMontantnegatif();
        }
        else {
            this.Solde = this.Solde - montant;
            A.Solde = A.Solde + montant;
            System.out.println("le virement à bien été effectué");
        }
    }
}
