public final class Fraction {

    private final int numerateur;
    private final int denominateur;

    public Fraction(int numerateur, int denominateur){
        this.denominateur=denominateur;
        this.numerateur=numerateur;
    }

    public Fraction(int numerateur){
        this.numerateur=numerateur;
        this.denominateur=1;
    }

    public Fraction ZERO = new Fraction(0,1);
    public Fraction UN = new Fraction(1,1);

    public Fraction(){
        this.denominateur=1;
        this.numerateur=1;
    }

    public int getnumerateur()
    {
        return numerateur;
    }

    public int getdenominateur()
    {
        return denominateur;
    }

    public double getfractiondouble(){
        return this.getnumerateur()/this.getdenominateur();
    }

    public Fraction additionfraction(Fraction F1){
        int n,d;
        if(this.denominateur==F1.denominateur){
            n=this.numerateur+F1.numerateur;
            d=this.denominateur;
        }
        else{
            d= this.denominateur*F1.denominateur;
            n=(this.numerateur*F1.denominateur)+(F1.numerateur*this.denominateur);
        }
        return new Fraction(n,d);
    }

    public boolean egalite(Fraction F){
        if(this.getfractiondouble()==F.getfractiondouble()){
            return true;
        }
        return false;
    }

    public void comparaison(Fraction F1){
        if(this.getfractiondouble()>F1.getfractiondouble()){
            System.out.println("plus grand");
        }
        else{
            if (this.getfractiondouble()==F1.getfractiondouble()){
                System.out.println("égale");
            }
            else{
                System.out.println("plus petit");
            }
        }
    }

    @Override
    public String toString() {
        String n = Integer.toString(this.numerateur);
        String d = Integer.toString(this.denominateur);

        return n + " sur " + d;
    }
}
